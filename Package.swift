// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import Foundation

import PackageDescription

let package = Package(
    name: "SwiftRoutes",
    platforms: [.iOS(.v11)],
    products: [
        .library(
            name: "SwiftRoutes",
            targets: ["SwiftRoutes"]),
    ],
    dependencies: [],
    targets: [
        .target(
            name: "SwiftRoutes",
            dependencies: [],
            exclude: ["Info.plist"],
            resources: [.process("Resources")]),
        .testTarget(
            name: "SwiftRoutesTests",
            dependencies: ["SwiftRoutes"],
            exclude: ["Info.plist"]),
    ]
)
